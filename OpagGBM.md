# Menú Ordenes

## Ordenes Pagadas (Deposit)

Este menú nos ayuda a visualizar las Ordenes que ya han sido pagadas de tipo Deposit.

#### Consultar una Orden

La consulta de Ordenes Pagadas se puede realizar mediante las siguientes opciones de filtro:

- **Empresas:** en esta lista se muestran las empresas que están dados de alta en el sistema.
- **Bancos:** en esta lista se muestran los bancos que están dados de alta en el sistema.
- **Fecha especifica:** seleccionar el periodo de tiempo.

![](/img/Ord_pag.png)


Si la búsqueda tuvo resultados, estos se muestran en una tabla, de lo contrario no se muestra ninguna información.

![](/img/Ord_pag_gbm.png)

**CON ESTO SE TERMINA EL PROCESO DE CONSULTA DE ORDENES PAGADAS**


**HERRAMIENTAS**

Esta opción sirve para poder exportar la información de la tabla a un archivo en Excel o a XML.

- Dar clic en ***Herramientas***.
- Seleccionar el modo de descarga *Exportar a Excel* o *Exportar a XML*
- Esperar a que descargue el archivo.
- Ejemplo del resultado de la descarga:

>![](/img/Her_pag_exc.png)
