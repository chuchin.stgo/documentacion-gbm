# Menú Ordenes

## Ordenes Pendientes(DEPOSIT)

Este menú nos ayuda a visualizar todas las ordenes en estatus Pendiente de tipo: Deposito SPEI, que se encuentran en la base de datos.

>**NOTA:** Para poder visualizar todas las ordenes que el sistema tiene para este usuario, en el menu desplegable de Empresas hacer clic en seleccionar una opción y despues hacer clic en Buscar, si se necesita hacer una búsqueda especifica, seleccionar la empresa a consultar.

### Consultar una Orden

Cuando la lista de ordenes pendientes es muy grande el usuario puede realizar una búsqueda especifica, mediante los filtros siguientes:


![](/img/Ord_pen.png)

Si la búsqueda tuvo resultados, esto se presentan en un tabla de lo contrario se muestra una tabla sin ningún registro.


### Enviar Ordenes a GBM

El sistema te permite enviar a GBM varias ordenes a la vez. Existen algunas restricciones como:

- Revisar que el **Banco** sea la correcta.
- Revisar que el **Numero de cuenta** sea la correcta.
- Revisar que las ordenes se paguen a **Personas Fisicas** y no a **Empresas**.
- Las ordenes retenidas no se envían a GBM.
- Seleccionar la empresa por donde se enviaran las ordenes (Inpamex1, Inpamex2, Inpamex3) 

![](/img/Ord_pen_gbm.png)



#### Al dar clic en Enviar a GBM las ordenes se envían a Ordenes en Proceso (Deposit).

![](/img/gbm.png)


