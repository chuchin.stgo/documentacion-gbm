# INPAMEX

## Página Principal

Esta página se muestra cuando inicia sesión el usuario con el rol de **SPEI.**

El sistema automáticamente reconoce que privilegios y cuales son las acciones que este usuario puede hacer en el sistema.

El usuario con el rol de **SPEI** encontrará las siguientes opciones de navegación:

![](/img/inpa.png)



