# Menú Ordenes

## Ordenes en Proceso (DEPOSIT)

Este menú nos ayuda a visualizar todas las ordenes en estatus **En Proceso** de tipo Deposito.

Al entrar a esta página el sistema te muestra las ordenes de la empresa que esta seleccionada.

>**Nota:** Para poder visualizar todas las ordenes que el sistema tiene para este usuario, no seleccionar ninguna opción en el filtro de búsqueda, si se necesita hacer una búsqueda especifica seleccionar la empresa.
 
![](/img/Ord_pro.png)

Si la búsqueda tuvo resultados, estos se muestran en una tabla, de lo contrario no se muestra ninguna información.

![](/img/Ord_pro_gbm.png)

**CONFIRMACIÓN**
El proceso de Confirmación y Rechazo de las ordenes se realizara de forma automatica por el sistema de GBM, sin que se tengan que ingresar los datos en rechazar o el codigo de confirmacion en el campó de orden a pagar.


>**NOTA:** Con esto termina el proceso de Confirmación de una orden.

---


**HERRAMIENTAS**

Esta opción sirve para poder exportar la información de la tabla a un archivo en Excel.

- Dar clic en ***Herramientas***.
- Seleccionar el modo de descarga *Exportar a Excel*.
- Esperar a que descargue el archivo.
- Ejemplo del resultado de la descarga:

>![](/img/Her_exc.png)
